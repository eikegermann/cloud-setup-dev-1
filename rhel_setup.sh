#!/bin/bash

# # Add this into userdata
# sudo yum update -y
# sudo yum install -y git
# git clone https://bitbucket.org/ghandic/cloud-setup-dev.git
# cd cloud-setup-dev
# bash rhel_setup.sh

S3_BUCKET_PUB_KEYS="cg-apac-invent-sva-docclass-dev-machine-setup"

sudo dnf install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
sudo dnf config-manager --set-enabled codeready-builder-for-rhel-8-rhui-rpms
sudo yum install -y epel-release
sudo yum install -y git-lfs docker python3-pip
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o docker-compose
sudo mv docker-compose /usr/local/bin && sudo chmod +x /usr/local/bin/docker-compose
sudo git lfs install
sudo pip3 install boto3
sudo python3 rhel_create_users.py --bucket $S3_BUCKET_PUB_KEYS