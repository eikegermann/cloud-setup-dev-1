import os
import logging

import boto3

logger = logging.getLogger()

roles2groups = {"admin": ["sudo", "docker"], "developer": ["docker"], "guest": []}


def get_users_keys(bucket_name):
    s3 = boto3.resource("s3")
    my_bucket = s3.Bucket(bucket_name)
    files = my_bucket.objects.all()
    user_list = []
    for file in files:
        if file.key.endswith(".pub"):
            username = os.path.basename(os.path.dirname(file.key))
            role = os.path.basename(os.path.dirname(os.path.dirname(file.key)))
            data = {
                "role": role,
                "name": username,
                "s3_path": file.key,
                "public_key": s3.Object(bucket_name, file.key).get()["Body"].read().decode("utf-8"),
            }
            user_list.append(data)
    return user_list


class User(object):
    def __init__(self, name):
        self.name = name

    def create(self):
        logger.info(f"Creating user {self.name}")
        os.system(f"adduser {self.name} --disabled-password --gecos ''")

    def add_to_group(self, group):
        logger.info(f"Adding user: {self.name} to group: {group}")
        os.system(f"usermod -aG {group} {self.name}")

    def add_ssh_key(self, key):
        ssh_dir = f"/home/{self.name}/.ssh/"
        ssh_authorized_keys = os.path.join(ssh_dir, "authorized_keys")
        logger.info(f"Adding public SSH key for user: {self.name} into {ssh_authorized_keys}")
        os.makedirs(ssh_dir, exist_ok=True)
        with open(ssh_authorized_keys, "w") as f:
            f.write(key)

    def add_to_passwordless_sudo(self):
        with open(f"/etc/sudoers.d/{self.name}", "w") as f:
            f.write(f"{self.name} ALL=(ALL) NOPASSWD: ALL")


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--bucket", required=True, type=str, help="Bucket to pull users public keys from")
    args = parser.parse_args()

    user_list = get_users_keys(args.bucket)
    print(user_list)
    for user in user_list:
        u = User(user["name"])
        u.create()
        u.add_ssh_key(user["public_key"])
        for allowed_group in roles2groups.get(user["role"], []):
            u.add_to_group(allowed_group)
            if allowed_group == "sudo":
                u.add_to_passwordless_sudo()
